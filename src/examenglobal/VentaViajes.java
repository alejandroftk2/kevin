/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenglobal;

/**
 *
 * @author elpep
 */
public class VentaViajes {
    private int numBoleto = 0;
    private String nomCliente = "";
    private int añosCum = 0;
    private String destino = "";
    private int tipoViaje = 0;
    private float precio = 0.0f;

     public VentaViajes() {
    
        this.numBoleto = 0;
        this.nomCliente = "";
        this.destino = "";
        this.añosCum = 0;
        this.precio = 0.0f;
        this.tipoViaje = 0;
        
    }

    public VentaViajes(int numBoleto, String nomCliente, String destino, int añosCum, float precio, int tipoViaje) {
        this.numBoleto = numBoleto;
        this.nomCliente = nomCliente;
        this.destino = destino;
        this.añosCum = añosCum;
        this.precio = precio;
        this.tipoViaje = tipoViaje;
        
    }
    
    public VentaViajes(VentaViajes copia) {
        this.numBoleto = copia.numBoleto;
        this.nomCliente = copia.nomCliente;
        this.destino = copia.destino;
        this.añosCum = copia.añosCum;
        this.precio = copia.precio;
        this.tipoViaje = copia.tipoViaje;

    }

    public int getNumBoleto() {
        return numBoleto;
    }

    public void setNumBoleto(int numBoleto) {
        this.numBoleto = numBoleto;
    }

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    public int getAñosCum() {
        return añosCum;
    }

    public void setAñosCum(int añosCum) {
        this.añosCum = añosCum;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public int getTipoViaje() {
        return tipoViaje;
    }

    public void setTipoViaje(int tipoViaje) {
        this.tipoViaje = tipoViaje;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public float calcularPrecioBoleto(){
        float bol = 0.0f;
        bol = this.precio * 1.8f;
        return bol;
    }
    
    public float calcularImpuesto(){
     float imp = 0.0f;
        imp = this.precio * 0.16f;
        return imp;
    }
    
    public float calcularDescuento(){
        float edad = 0.0f;
        if (this.añosCum >= 60)
            edad = this.calcularPrecioBoleto() * .5f;
        else
            edad = 0;
        return edad; 
            
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = (this.calcularPrecioBoleto() + this.calcularImpuesto() ) - this.calcularDescuento();
        return total;
   }
}
